﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{

    public Player MainScript;

    public ChunkRenderer chunkRenderer;

    public GroundDetector groundDetector;

    public Rigidbody Rigidbody;

    private const float ZERO = 0;

    public float walkSpeed = 6;
    public float runSpeed = 10;
    public float strafeSpeed = 5;
    public float gravity = 20;
    public float jumpHeight = 2;
    public bool canJump = true;
    public bool isRunning = false;

    public bool firstButtonPressed = false;
    public bool reset = false;
    public float timeOfFirstButton = 0;

    public float flyingUpDown_speed = 5;

    public int stopMomentum;

    public bool isFlyingDown = false;

    public bool flying;

    void Start()
    { 

        //Rigidbody = GetComponent<Rigidbody>();
        Rigidbody.freezeRotation = true;
        Rigidbody.useGravity = false;
    }

    void Update()
    {
        //print("FixedUpdate");
        if (!MainScript.inventory.isBackpackOpen && !MainScript.chunkMngr.loadingScrrenOpen && !MainScript.escapeMenu && !MainScript.chat.inputField.isFocused && !MainScript.Dead) // if menu is closed and im not loading a new level
        {
            if (Rigidbody.isKinematic) // if movment is disabled set it to true
            {
                Rigidbody.isKinematic = false;
            }

            if (flying && Data.GameMode_int != 1)
            {
                flying = false;
            }

            if (!groundDetector.ExecuteCode)
            {
                groundDetector.ExecuteCode = true;
            }

            // get correct speed
            float forwardAndBackSpeed = walkSpeed;

            // if running, set run speed
            if (isRunning)
            {
                forwardAndBackSpeed = runSpeed;
            }

            // calculate how fast it should be moving
            Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal") * strafeSpeed, 0, Input.GetAxis("Vertical") * forwardAndBackSpeed);
            targetVelocity = transform.TransformDirection(targetVelocity);

            // apply a force that attempts to reach our target velocity
            Vector3 velocity = Rigidbody.velocity;
            Vector3 velocityChange = (targetVelocity - velocity);
            velocityChange.y = 0;
            Rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);

            // jump
            if (canJump && groundDetector.IsGrounded && Input.GetButton("Jump"))
            {
                Rigidbody.velocity = new Vector3(velocity.x, Mathf.Sqrt(2 * jumpHeight * gravity), velocity.z);
                groundDetector.IsGrounded = false;
            }


            if (Input.GetButtonDown("Jump") && firstButtonPressed && Data.GameMode_int == 1 && Data.DoubleSpaceToFly)
            {
                if (Time.time - timeOfFirstButton < 0.9f)
                {
                    ToggleFlight();
                    timeOfFirstButton = 0;
                    Debug.Log("DoubleClicked " + flying);
                }
                else
                {
                    Debug.Log("Too late");
                }
                reset = true;
            } // get double jump to fly
            if (Input.GetButtonDown("Jump") && !firstButtonPressed && Data.GameMode_int == 1 && Data.DoubleSpaceToFly)
            {
                firstButtonPressed = true;
                timeOfFirstButton = Time.time;
            }
            if (reset)
            {
                firstButtonPressed = false;
                reset = false;
            } // end /\

            if (flying && Input.GetButton("Jump"))
            {
                Rigidbody.velocity = new Vector3(Rigidbody.velocity.x, flyingUpDown_speed, Rigidbody.velocity.z);
                stopMomentum = 5;
            } // if flying and space, go up
            if (flying && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)))
            {
                Rigidbody.velocity = new Vector3(Rigidbody.velocity.x, -flyingUpDown_speed, Rigidbody.velocity.z);
                stopMomentum = 5;
                isFlyingDown = true;
            } // if flying and ctrl, go down
            else
            {
                isFlyingDown = false;
            }
        }
        else
        {
            Rigidbody.velocity = new Vector3(0, Rigidbody.velocity.y, 0);
            if (groundDetector.ExecuteCode)
                groundDetector.ExecuteCode = false;
        }

        if (!flying && !MainScript.Dead)
        {
            // apply gravity
            Rigidbody.AddForce(new Vector3(0, -gravity * Rigidbody.mass, 0));
        }

        if (isFlyingDown && groundDetector.IsGrounded)
        {
            flying = false;
        }

        // check if the player is running
        if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
        {
            isRunning = true;
        }

        // check if the player stops running
        if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift))
        {
            isRunning = false;
        }

    }

    public void ToggleFlight() // otggle flight
    {
        flying = !flying;
        Rigidbody.velocity = Vector3.zero;
    }
    public void ToggleFlight(bool mode) // set flight to mode
    {
        flying = mode;
        Rigidbody.velocity = Vector3.zero;
    }
}