﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

    public Player MainScript;

    public GameObject[] Hearts = new GameObject[20];

    public GameObject[] Backgrounds = new GameObject[10]; // every one is in back of two hearts

    public int CurrentHealth;

	// Use this for initialization
	void Start () {
        CurrentHealth = Data.Health;
	}
	
	// Update is called once per frame
	void Update () {
		if (Data.Health != CurrentHealth)
        {
            CurrentHealth = Data.Health;
            UpdateHealthGraphics();
        }
        
	}

    public void UpdateHealthGraphics()
    {
        for (int i = 0; i < Hearts.Length; i++)
        {
            if (i > CurrentHealth-1)
            {
                Hearts[i].SetActive(false);
            }
            else
            {
                Hearts[i].SetActive(true);
            }
        }
    }
}
