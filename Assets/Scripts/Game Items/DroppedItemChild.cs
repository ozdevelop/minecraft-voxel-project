﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroppedItemChild : MonoBehaviour
{

    public DropedItem parent;

    public float counter = 1;

    void Update()
    {
        if (counter > 0)
        {
            counter -= Time.deltaTime;
        }
    }

    void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "DroppedItem" && counter <= 0)
        {
            if (collider.GetComponent<DropedItem>() != null && collider.GetComponent<DropedItem>().myType != null && collider.GetComponent<DropedItem>().myType.Item.Count > 0 && collider.GetComponent<DropedItem>().myType.Item.Peek().type != BlockData.BlockTypes.NONE)
            {
                try
                {
                    DroppedItemData other = collider.GetComponent<DropedItem>().myType;
                    if (other.Item.Count > 0 && other.Item.Peek().type == parent.myType.Item.Peek().type)
                    {
                        parent.myType.AddItems(parent.myType.Item.Peek(), other.Count);
                        Destroy(collider.gameObject);
                    }
                }
                catch (System.Exception E)
                {
                    print("Exception: " + E.ToString());
                }
                counter = 1;
            }
        }
    }
}