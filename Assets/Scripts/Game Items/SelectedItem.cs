﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedItem : MonoBehaviour {

    public Renderer rend;
    public BlockData.BlockTypes type;
	// Use this for initialization
	void Start () {
        if (rend == null)
            rend = GetComponent<Renderer>();
	}
}
